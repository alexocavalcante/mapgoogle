package com.example.mapapplication;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap mMap;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.stylegta));

        // Add a marker in Sydney and move the camera
        LatLng p1 = new LatLng(-3.11712997, -59.99406338);
        LatLng p2 = new LatLng(-3.11967965, -59.99499679);
        LatLng p3 = new LatLng(-3.11630507, -59.99482512);
        LatLng p4 = new LatLng(-3.11626222, -59.99652028);
        LatLng p5 = new LatLng(-3.11801914, -59.99653101);

        mMap.addMarker(new MarkerOptions().position(p1).title("Coleta 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.recycle)));
        mMap.addMarker(new MarkerOptions().position(p2).title("Coleta 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.recycle)));
        mMap.addMarker(new MarkerOptions().position(p3).title("Coleta 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.recycle)));
        mMap.addMarker(new MarkerOptions().position(p4).title("Coleta 4").icon(BitmapDescriptorFactory.fromResource(R.drawable.recycle)));
        mMap.addMarker(new MarkerOptions().position(p5).title("Coleta 5").icon(BitmapDescriptorFactory.fromResource(R.drawable.recycle)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(p1, 16.0f));


        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            mMap.setMyLocationEnabled(true);
            return;
        }

    }
}
